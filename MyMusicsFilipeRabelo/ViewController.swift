//
//  ViewController.swift
//  MyMusicsFilipeRabelo
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit
    
struct Music {
    let musicName: String
    let albumName: String
    let singerName: String
    let smallimageName: String
    let bigimageName: String
}



class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var musicList:[Music] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.musicList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! MyCell
        let music = self.musicList[indexPath.row]
        
        cell.music.text = music.musicName
        cell.album.text = music.albumName
        cell.singer.text = music.singerName
        cell.coverart.image = UIImage(named: music.smallimageName)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "openDetail", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailsViewController = segue.destination as! MusicDetailViewController
        let index = sender as! Int
        let music = self.musicList[index]
        
        detailsViewController.Imagename = music.bigimageName
        detailsViewController.Musicname = music.musicName
        detailsViewController.Albumname = music.albumName
        detailsViewController.Singername = music.singerName
    }
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.musicList.append(Music(musicName: "Pontos Cardeais", albumName: "Álbum Vivo!", singerName: "Alceu Valença", smallimageName: "capa_alceu_pequeno", bigimageName: "capa_alceu_grande"))
        self.musicList.append(Music(musicName: "Menor Abandonado", albumName: "Álbum Patota de Cosme", singerName: "Zeca Pagodinho", smallimageName: "capa_zeca_pequeno", bigimageName: "capa_zeca_grande"))
        self.musicList.append(Music(musicName: "Tiro ao Álvaro", albumName: "Álbum Adoniran Barbosa e Convidados", singerName: "Adoniran Barbosa", smallimageName: "capa_adoniran_pequeno", bigimageName: "capa_adhoniran_grande"))
        
        
    }


}
