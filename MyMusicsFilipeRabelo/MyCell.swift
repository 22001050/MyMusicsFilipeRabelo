//
//  MyCell.swift
//  MyMusicsFilipeRabelo
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var coverart: UIImageView!
    @IBOutlet weak var music: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var singer: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
