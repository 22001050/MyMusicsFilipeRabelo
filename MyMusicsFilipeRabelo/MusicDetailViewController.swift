//
//  MusicDetailViewController.swift
//  MyMusicsFilipeRabelo
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class MusicDetailViewController: UIViewController {
    
    var Imagename: String = ""
    var Musicname: String = ""
    var Albumname: String = ""
    var Singername: String = ""
    
    @IBOutlet weak var coverart: UIImageView!
    @IBOutlet weak var music: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var singer: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.coverart.image = UIImage(named: self.Imagename)
        self.music.text = self.Musicname
        self.album.text = self.Albumname
        self.singer.text = self.Singername
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
